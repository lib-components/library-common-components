import { createWebHistory, createRouter } from "vue-router";
import Home from "@/pages/Home";
import Alerts from "@/pages/Alerts";
import Modals from "@/pages/Modals";
import Tables from "@/pages/Tables";
import Buttons from "@/pages/Buttons";
import Tooltips from "@/pages/Tooltips";
import Textarea from "@/pages/Textareas";
import Inputs from "@/pages/Inputs";
import Tabs from "@/pages/Tabs";
import Switchers from "@/pages/Switchers";
import Selects from "@/pages/Selects";
import Loaders from "@/pages/Loaders";

const routes = [
  {
    path: "/",
    name: "",
    component: Home,
  },
  {
    path: "/alerts",
    name: "Alerts",
    component: Alerts
  },
  {
    path: "/modals",
    name: "Модальные окна",
    component: Modals
  },
  {
    path: "/buttons",
    name: "Кнопки",
    component: Buttons
  },
  {
    path: "/tooltips",
    name: "Тултипы и подсказки",
    component: Tooltips
  },
  {
    path: "/tables",
    name: "Таблицы",
    component: Tables
  },
  {
    path: "/textarea",
    name: "Текстариа",
    component: Textarea
  },
  {
    path: "/inputs",
    name: "Инпуты",
    component: Inputs
  },
  {
    path: "/tabs",
    name: "Табсы",
    component: Tabs
  },
  {
    path: "/switchers",
    name: "Переключатели",
    component: Switchers
  },
  {
    path: "/selects",
    name: "Выпадашки",
    component: Selects
  },
  {
    path: "/loaders",
    name: "Лоадеры и загрузки",
    component: Loaders
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;